" Classic Janus mappings
nmap <leader>gb :Git blame<CR>
nmap <leader>gs :Git<CR>
nmap <leader>gd :Gdiff<CR>
nmap <leader>gc :Git commit<CR>

" Shortcut to load commit history in quickfix
" Limit number commits in quickfix to avoid slow load of long repo histories
" https://github.com/tpope/vim-fugitive/issues/1662
let g:lam_log_limit = 300
execute "nmap <leader>gl :Gclog -" . g:lam_log_limit . "<CR>"
execute "vmap <leader>gl :Gclog -" . g:lam_log_limit . "<CR>"

" Shortcut browsing in the repo hosting provider
nmap <leader>gh :GBrowse<CR>
vmap <leader>gh :GBrowse<CR>

" Shortcut to push and force push
nmap <leader>gp :Git push<CR>
" TODO Avoid conflict with gf?
nmap <leader>gfp :Git push -f<CR>

" Shortcut for saving the current branch to origin
command! GSetUpstream execute "Git push -u origin" FugitiveHead()
nmap <leader>gsu :GSetUpstream<CR>

" Shortcut for editing a fugitive object
nmap <leader>ge :Gedit<CR>

" TODO how to get current branch name
" TODO in mergetool: show message when there are no more conflicts

let g:EditorConfig_exclude_patterns = ['fugitive://.*']  " Keep editor config from conflicting with fugitive
let g:gist_post_private = 1                              " Make private gists by default

function! lam#ClearNewline(string)
  return strtrans(substitute(a:string, '\n\+', '', ''))
endfunction

" Show the fugitive log for the current feature branch
function! lam#GitDefaultBranch()
  return lam#ClearNewline(execute("Git config --get init.defaultBranch"))
endfunction

function! lam#GitDefaultRemote()
  return lam#ClearNewline(execute("Git rev-parse --abbrev-ref " . lam#GitDefaultBranch() . "@{upstream}"))
endfunction

function! lam#GitMergeBase(branch = lam#GitDefaultRemote())
  return lam#ClearNewline(execute("Git merge-base HEAD " . a:branch))
endfunction

command! GlogChanges execute "Gclog " . lam#GitMergeBase() . "..HEAD"
nmap <leader>gg :GlogChanges<CR>

" Deprecations
command! GlogMergeBase echoerr "Use GlogChanges instead"
nmap <leader>gm :echoerr "gm mapping deprecated: Use gg instead" \| GlogChanges<CR>

" Interactive rebase onto the default branch
command! RebaseOntoDefault execute "Git rebase -i " . lam#GitDefaultRemote()
nmap <leader>gr :RebaseOntoDefault<CR>
" TODO Add rebase onto merge base

" Load stashes in the quickfix list
" Ref: https://github.com/tpope/vim-fugitive/issues/236#issuecomment-635628157
nmap <leader>gz :Gclog -g stash<CR>

" Map view fugitive reflog to fugitive prefix + backspace
nmap <leader>g<BS> :Git reflog<CR>
